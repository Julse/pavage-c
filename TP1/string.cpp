#include <iostream>
#include <cstring>
#include <cstdlib>

class String {
	private:
		char *data;
		unsigned int size;
	public:
		String(const char* s="")
		{
			size = strlen(s);
			data = new char[size+1];
			strcpy(data, s);
		}
		
		String(const char c)
		{
			size=1;
			data = new char[size+1];
			data[0]=c;
			data[1]=0;
		}
		
		const char* to_char() const
		{
			return data;
		}
		
		operator const char*() const
		{
			return to_char();
		}
		
		String operator+(const String& s2) const
		{
			char* res;
			int tmp_size = 0;
			tmp_size = size + s2.size + 1;
			res = new char[tmp_size];
			strcpy(res, data);
			strcat(res, s2);
			
			return String(res);
		}
		
		bool operator==(const String& s) const
		{
			if (size != s.size){
				return false;
			}
			
			for (unsigned int i=0; i<s.size; i++){
				if (s.data[i] != data[i]){
					return false;
				}
			}
			
			return true;
		}
		
		bool operator!=(const String& s) const
		{
			if (size != s.size){
				return true;
			}
			
			for (unsigned int i=0; i<s.size; i++){
				if (s.data[i] != data[i]){
					return true;
				}
			}
			
			return false;
		}
		
		bool operator<(const String& s) const
		{
			return strcmp(data, s.data) < 0;
		}
		
		bool operator>(const String& s) const
		{
			return strcmp(data, s.data) > 0;
		}
		
		bool operator<=(const String& s) const
		{
			return strcmp(data, s.data) <= 0;
		}
		
		bool operator>=(const String& s) const
		{
			return strcmp(data, s.data) >= 0;
		}
		
		char& operator[](std::size_t idx)
		{
			if (idx > size)
			{
				std::cout << "Out of bound !!!!!" << std::endl;
				abort();
			}
			return data[idx];
		}
		
		const char& operator[](std::size_t idx) const
		{
			if (idx > size)
			{
				std::cout << "Out of bound !!!!!" << std::endl;
				abort();
			}
			return data[idx];
		}
		
		int get_size()
		{
			return size;
		}

		bool empty()
		{
			return size == 0;
		}

		String substr(std::size_t a, std::size_t b)
		{
			if (a < 0 || a > size || b < 0 || b > size || a > b)
			{
				std::cerr << "Wrong indices" << std::endl;
				abort();
			}
			char *d = new char[b - a + 1];
			strncpy(d, data+a, b-a);
			d[b -a] = 0;
			return String(d);
		}

		String remove(const char* str)
		{
			char *t = strstr(data, str);
			if (t == 0) // pas de matching
			{
				return String(data);
			}

			// Occurence trouvée
			char *d = new char[size - strlen(str) + 1];
			strncpy(d, data, t-data);
			strcat(d, t+strlen(str));
			return String(d);

		}
		
		friend std::ostream & operator << (std::ostream & os, const String& lc);
		friend std::istream & operator >> (std::istream & is, const String& lc);
};

std::ostream & operator << (std::ostream & os, const String& lc) {
	os << lc.data;
	return os;
}

std::istream & operator >> (std::istream & is, const String& lc) {
	
}

int main() {
	String vide;
	String aaa("aaa");
	String abcd("abcd");
	String b('b');

	
	std::cout << vide << std::endl;
	std::cout << aaa << std::endl;
	std::cout << b << std::endl;
	
	
	std::cout << strlen(aaa) << std::endl;
	
	String cat = aaa + b;
	std::cout << cat << " " << cat.get_size() << std::endl;

	if (aaa == aaa)
		std::cout << "aaa egal a aaa" << std::endl;
		
	
	if (aaa != b)
		std::cout << "aaa pas egal a b" << std::endl;
		
	if (aaa > b)
		std::cout << "a > b" << std::endl;
		
	if (b > aaa)
		std::cout << "b > aaa" << std::endl;
		
	if (aaa < b)
		std::cout << "a < b" << std::endl;
		
	if (b < aaa)
		std::cout << "b < aaa" << std::endl;
		
	if (aaa <= aaa)
		std::cout << "aaa <= aaa" << std::endl;
		
	if (b >= b)
		std::cout << "b >= b" << std::endl;
		
	std::cout << abcd[2] << std::endl;
	//abcd[180000] = 'x';
	//std::cout << abcd[180000] << std::endl;

	String z = abcd.substr(2, 3);
	std::cout << z << std::endl;
//	String zz = abcd.substr(2, 9);
//	std::cout << zz << std::endl;


	String sample("Bonjour comment ca va la dedans");
	String f = sample.remove("comment");
	std::cout << sample << std::endl;
	std::cout << f << std::endl;
}
