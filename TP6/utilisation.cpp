#include <deque>
#include <set>
#include <list>
#include <iterator>
#include <iostream>
#include <algorithm>
#include "objetfunc.hpp"

int main()
{
	std::deque<int> d;
	Generateur g(4);
	d.push_back(g());
	d.push_back(g());
	d.push_back(g());
	d.push_back(g());
	d.push_back(g());
	
	std::ostream_iterator<int> oi(std::cout, " ");
	std::copy(d.begin(), d.end(), oi);
	
	std::cout << std::endl;
	
	std::set<int> s(d.begin(), d.end());
	std::copy(s.begin(), s.end(), oi);
	
	std::cout << std::endl;

	d.erase(d.begin(), d.end());
	std::copy(d.begin(), d.end(), oi);
	std::cout << "Taille deque : " << d.size() << std::endl;
	
	std::set<int> s2;
	Generateur g2(2);
	for (int i=0; i < 10; i++)
		s2.insert(g2());
		
	std::copy(s2.begin(), s2.end(), oi);
	std::cout << std::endl;

	std::set<int> s3;
	std::insert_iterator<std::set<int> > ins(s3,s3.begin());
	generate_n(ins, 6, Generateur(3));
	
	std::copy(s3.begin(), s3.end(), oi);
	std::cout << std::endl;
	
	std::ostream_iterator<int> oi2(std::cout, ";");
	std::copy(s.begin(), s.end(), oi2);
	std::cout << std::endl;
	std::copy(s2.begin(), s2.end(), oi2);
	std::cout << std::endl;
	std::copy(s3.begin(), s3.end(), oi2);
	std::cout << std::endl;
	
	std::list<int> l;
	std::insert_iterator<std::list<int> > ins2(l,l.begin());
	std::set_intersection(s.begin(), s.end(), s2.begin(), s2.end(), ins2);
	std::copy(l.begin(), l.end(), oi);
	std::cout << std::endl;
	
	std::set_union(s.begin(), s.end(), s3.begin(), s3.end(), oi);
}
