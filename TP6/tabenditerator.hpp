#ifndef ITER_END
#define ITER_END

template<typename T, std::size_t S>
class Tableau;

template<typename T, std::size_t S>
class TabEndIter
{
	private:
		Tableau<T, S>* tab;
	public:
		TabEndIter(Tableau<T, S>* t) : tab(t) {}
		

		Tableau<T, S>& operator=(const T& element)
		{	
			tab->ajoute(element);
			return *tab;
		}
};

#endif
