#include <iostream>

class Generateur
{
	int base;
	int c;
	
	public:
	Generateur(int b)
	{
		base = b;
		c = 0;
	}
	
	int operator()()
	{
		return c++ * base;
	}
};
