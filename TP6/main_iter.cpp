#include <iostream>
#include <string>

#include "tableaux.hpp"
#include "tabiterator.hpp"
#include "tabenditerator.hpp"


template<typename T>
void swap(T& a, T& b)
{
	T tmp = a;
	a = b;
	b = tmp;
}

template<typename T, unsigned int N>
void swap(T(&a)[N], T(&b)[N])
{
	for(int i=0; i < N; i++) {
		swap(a[i], b[i]);
	}
}

template<typename T, std::size_t S>
void tamiser(Tableau<T,S>& tab, int idx, std::size_t n){
   
   int k = idx;
   int j = 2*k;
   while (j <= n){
      if (j < n && tab[j] < tab[j+1])
         j = j+1;
      
      if (tab[k] < tab[j]){
         swap(tab[k],tab[j]);
         k = j;
         j = 2*k;
      }
      else
	break;
   }
}

template<typename T, std::size_t S>
void triParTas(Tableau<T,S>& tab, std::size_t n){
   for(int i = n/2; i >= 0; i--)
       tamiser(tab, i, n);
       
   for(int i = n-1; i >= 1; i--){
       swap(tab[i],tab[0]);
       tamiser(tab, 0, i-1);
   }
}



int main() {
	Tableau<std::string, 8> t;
	t.ajoute("aaa");
	t.ajoute("bbb");
	t.ajoute("ccc");
	
	Tableau<int, 8> e;
	e.ajoute(1);
	e.ajoute(2);
	e.ajoute(3);
	
	Tableau<int, 8> zz;
	zz.ajoute(10);
	zz.ajoute(20);
	zz.ajoute(30);
	zz.ajoute(5);
	zz.ajoute(4);
	zz.ajoute(1);
	zz.ajoute(12);
	zz.ajoute(4530);
	
	
	Tableau<std::string, 8> t2;
	t2.ajoute("aaa");
	
	TabIterator<std::string, 8> i = t.begin();
	
	std::cout << *i << std::endl;
	std::cout << i->size() << std::endl;
	
	++i;
	std::cout << *i << std::endl;
	std::cout << *(i++) << std::endl;
	std::cout << *i << std::endl;
	
	i = t.begin();
	//TabIterator<std::string, 8> i2 = t2.begin();
	Tableau<std::string, 8>::iterator i2 = t2.begin();
	
	if (i == i2)
		std::cout << "Egalite iterateur" << std::endl;
	
	TabIterator<std::string, 8> ite = t.end();
	for(;i!=ite;++i)
		std::cout << *i << std::endl;
		
	TabEndIter<int, 8> ite2(&e);
	ite2 = 3;
	
	std::cout << "ite2 : " << e << std::endl;
	
	
	std::cout << "zz : " << zz << std::endl;
	triParTas(zz, zz.get_nb());
	std::cout << "zz : " << zz << std::endl;	
	
	return 0;
}
