#ifndef TABLEAUX
#define TABLEAUX

#include <cstddef>
#include <iostream>
#include <cstdlib>
#include <cassert>

#include "tabiterator.hpp"

template<typename T, std::size_t S>
class Tableau
{
	private:
		T* data;
		
		std::size_t nb;
		std::size_t capacite;
		static unsigned int nb_instances;
	
	public:
		Tableau() : nb(0), capacite(S) 
		{
			data = new T[S];
			++nb_instances;
		}
		
		Tableau(const T t[], std::size_t taille) : nb(taille), capacite(taille+S)
		{
			data = new T[capacite];
			for(int i=0; i<nb; i++){
				data[i] = t[i];
			}
			++nb_instances;
		}
		
		Tableau(const Tableau<T, S>& autre) : nb(autre.nb), capacite(autre.capacite)
		{
			data = new T[capacite];
			for(int i=0; i<nb; i++){
				data[i] = autre[i];
			}
			++nb_instances;
		}
		
		void ajoute(T element);
		
		void augmente_capacite();
		
		std::size_t get_nb() const {
			return nb;
		}
		
		std::size_t get_capacite() const {
			return capacite;
		}
		
		unsigned int get_instances() const {
			return nb_instances;
		}
		
		T* get_data() const {
			return data;
		}

		T& operator[](unsigned int index)
		{
			if (index > nb)
			{
				std::cout << "Out of range !!" << std::endl;
				abort();
			}
			return data[index];
		}
		
		const T operator[](unsigned int index) const
		{
			if (index > nb)
			{
				std::cout << "Out of range !!" << std::endl;
				abort();
			}
			return data[index];
		}
		
		Tableau<T, S>& operator=(const Tableau<T, S>& autre) 
		{
			if(autre.capacite>capacite){
				delete[] data;
				data = new T[autre.capacite];
				capacite = autre.capacite;
				nb = autre.nb;
				for(int i=0; i<autre.nb; i++){
					data[i] = autre.data[i];
				}
			}  
			else {
				nb = autre.nb;
				for(int i=0; i<autre.nb; i++){
					data[i] = autre.data[i];
				}
			}
			
			return *this;
		}
		
		~Tableau(){
			std::cout << "destruction" << std::endl;
			delete[] data;
			--nb_instances;
		}
		
		TabIterator<T, S> begin() {
			return TabIterator<T, S>(this, 0);
		}
		TabIterator<T, S> end() {
			return TabIterator<T, S>(this, nb);
		}
		
		
		typedef TabIterator<T, S> iterator;
};

template<typename T, std::size_t S>
void Tableau<T, S>::ajoute(T element){
	if(nb>=capacite){
		augmente_capacite();
	}  
	//Juste pour essayer
	assert(nb<capacite);
	
	std::cout << "ajout de " << element << std::endl;
	data[nb]=element;
	nb++;
}

template<typename T, std::size_t S>
unsigned int Tableau<T, S>::nb_instances = 0;

template<typename T, std::size_t S>
void Tableau<T, S>::augmente_capacite(){
		std::cout << "reallocation" << std::endl;
		T* tmp_data = data;
		data = new T[nb+S];
		capacite = nb+S;
		for(int i=0; i<nb; i++){
			data[i] = tmp_data[i];
		}
		delete tmp_data;
}

template<typename T, std::size_t N>
std::ostream & operator << (std::ostream & os, const Tableau<T,N>& tab) {
	for (int i=0; i < tab.get_nb(); i++)
		os << tab.get_data()[i] << " ";
	os << std::endl;
	return os;
}


template<typename T, std::size_t N>
std::istream & operator >> (std::istream & is, Tableau<T,N>& tab) {
	
	T tmp;
	is >> tmp;
	tab.ajoute(tmp);

	return is;
}

#endif
