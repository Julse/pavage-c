#ifndef ITER
#define ITER

template<typename T, std::size_t S>
class Tableau;

template<typename T, std::size_t S>
class TabIterator
{
	private:
		Tableau<T, S>* tab;
		std::size_t idx;
	public:
		TabIterator(Tableau<T, S>* t, std::size_t i) : tab(t), idx(i) {}
		
		T& operator*()
		{
			return (*tab)[idx];
		}
		
		T* operator->()
		{
			return tab->get_data() + idx;
		}
		
		TabIterator<T,S>& operator++()
		{
			idx++;
			return *this;
		}
		
		TabIterator<T,S>& operator++(int i=0)
		{
			TabIterator<T,S> tmp(*this);
			idx++;
			return tmp;
		}
		
		bool operator==(TabIterator<T,S>& tab2)
		{
			return (this->idx == tab2.idx && this->tab == tab2.tab);
		}
		
		bool operator!=(TabIterator<T,S>& tab2)
		{
			return !(*this == tab2);
		}
};

#endif
