#include <iostream>
#include "expression.hpp"


Constante::~Constante(){ std::cout << "Destructeur Constante" << std::endl; }
int Constante::eval() const {
	return valeur;
}
Expression* Constante::clone() const{
	return new Constante(valeur);
}


Mult::Mult(const Expression & _expD, const Expression & _expG){
	expD=_expD.clone(); 
	expG=_expG.clone(); 
	std::cout << "Constructeur   Mult" << std::endl;}

Mult::~Mult() {
	delete expD; 
	delete expG;  
	std::cout << "Destructeur Mult" << std::endl;
}

int Mult::eval() const {
	return expD->eval()*expG->eval();
}

Expression * Mult::clone() const {
	return new Mult(*expD, *expG);
}

/*Mult::Mult(Expression && _expD, Expression && _expG) : expD(&_expD), expG(&_expG){
	std::cout << "Move Mult" << std::endl;
}*/

Plus::Plus(const Expression & _expD, const Expression & _expG)
{expD=_expD.clone(); expG=_expG.clone();  std::cout << "Constructeur   plus" << std::endl;}
//Plus::Plus(Expression && _expD, Expression && _expG) : expD(&_expD), expG(&_expG){std::cout << "Move PLus" << std::endl;}
Plus::~Plus() {delete expD; delete expG;  std::cout << "Destructeur plus" << std::endl;}

int Plus::eval() const {return expD->eval()+expG->eval();}

Expression * Plus::clone() const {return std::move(const_cast<Plus*>(this)); }


Moins::Moins(const Expression & _expD, const Expression & _expG)
{expD=_expD.clone(); expG=_expG.clone();  std::cout << "Constructeur   Moins" << std::endl;}
Moins::~Moins() {delete expD; delete expG;  std::cout << "Destructeur Moins" << std::endl;}
int Moins::eval() const {return expD->eval()-expG->eval();}
Expression * Moins::clone() const {return new Moins(*expD, *expG);}
//Moins::Moins(Expression && _expD, Expression && _expG) : expD(&_expD), expG(&_expG){std::cout << "Move Moins" << std::endl;}

MoinsUnaire::MoinsUnaire(const Expression & _exp){
	exp=_exp.clone();
	 std::cout << "Constructeur   MOinsUnaire" << std::endl;
}
MoinsUnaire::~MoinsUnaire() {delete exp;  std::cout << "Destructeur MOinsUnaire" << std::endl;}
int MoinsUnaire::eval() const {return -1*exp->eval();}
Expression * MoinsUnaire::clone() const {return new MoinsUnaire(*exp);}
//MoinsUnaire::MoinsUnaire(Expression && _exp) : exp(&_exp){std::cout << "Move MoinsUnaire" << std::endl;}
