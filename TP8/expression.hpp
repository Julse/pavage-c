#ifndef EXP_HPP // cette variable de compilation sert à ne pas inclure deux fois le fichier
#define EXP_HPP

#include <iostream>
#include <cstring>
//#include "expression.cpp"


class Expression {
	private:
	public:
		Expression() {std::cout << "Constructeur Expression" << std::endl;};
		virtual ~Expression() {std::cout << "Destructeur Expression" << std::endl;};
		virtual int eval() const = 0;
		virtual Expression* clone() const = 0;
};

class Constante : public Expression {
	private:
		int valeur;
	public:
		const Constante & operator=(Constante && x) {
			if (this != &x)
			{
				valeur = x.valeur;
			}
			return * this;
		}
		Constante (Constante &&c) : valeur(c.valeur){std::cout << "Move Constante" << std::endl;}
		Constante(int i=0) : valeur(i)
		{std::cout << "Constructeur Constante" << std::endl;}
		~Constante();
		int eval() const;
		Expression* clone() const;
};

class Mult : public Expression {
	private:
		//Mult* copie;
		Expression *expD;
		Expression *expG;
	public:
		Mult (Mult &&m) : expD(std::move(m.expD)), expG(std::move(m.expG)){std::cout << "Move Mult" << std::endl;}
		const Mult & operator=(Mult && x) {
			if (this != &x)
			{
				delete expD;
				delete expG;
				expD = x.expD;
				expG = x.expG;
				x.expD = nullptr;
				x.expG = nullptr;
			}
			return * this;
		}
		Mult(const Expression & _expD, const Expression & _expG);
		//Mult(Expression && _expD, Expression && _expG);
		~Mult();
		int eval() const;
		Expression * clone() const;
};

class Plus : public Expression {
	private:
		Expression *expD;
		Expression *expG;
	public:
		Plus (Plus &&m) : expD(m.expD), expG(m.expG){ m.expD = nullptr; m.expG = nullptr; std::cout << "Move Plus" << std::endl;}
		//Plus(Expression && _expD, Expression && _expG);
		const Plus & operator=(Plus && x) {
			if (this != &x)
			{
				delete expD;
				delete expG;
				expD = x.expD;
				expG = x.expG;
				x.expD = nullptr;
				x.expG = nullptr;
			}
			return * this;
		}
		Plus(const Expression & _expD, const Expression & _expG);
		~Plus();
		int eval() const;
		Expression * clone() const;
};

class Moins : public Expression {
	private:
		Moins* copie;
		Expression *expD;
		Expression *expG;
	public:
		Moins (Moins &&m) : expD(std::move(m.expD)), expG(std::move(m.expG)){std::cout << "Move Moins" << std::endl;}
		//Moins(Expression && _expD, Expression && _expG);
		const Moins & operator=(Moins && x) {
			if (this != &x)
			{
				delete expD;
				delete expG;
				expD = x.expD;
				expG = x.expG;
				x.expD = nullptr;
				x.expG = nullptr;
			}
			
			return * this;
		}
		Moins(const Expression & _expD, const Expression & _expG);
		~Moins();
		int eval() const ;
		Expression * clone() const;
};

class MoinsUnaire : public Expression {
	private:
		MoinsUnaire* copie;
		Expression *exp;
	public:
		MoinsUnaire (MoinsUnaire &&m) : exp(std::move(m.exp)){std::cout << "Move MoinsUnaire" << std::endl;}
		//MoinsUnaire(Expression && _expG);
		const MoinsUnaire & operator=(MoinsUnaire && x) {
			if (this != &x)
			{
				delete exp;
				exp = x.exp;
				x.exp = nullptr;
			}
			return * this;
		}
		MoinsUnaire(const Expression & _exp);
		~MoinsUnaire();
		int eval() const;
		Expression * clone() const;
};

#endif
