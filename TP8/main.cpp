#include "expression.hpp"
#include <iostream>


int main()
{
	int a=5;
	
	const Expression &e = Plus(Plus(Constante(a), Constante(1)), Plus(Constante(a), Constante(1)));
	
	//const Expression & e = Mult(std::move(Plus(std::move(Constante(a)), std::move(MoinsUnaire(std::move(Constante(-2)))))), std::move(Plus(std::move(Constante(1)), std::move(Constante(3)))));
	
	std::cout << e.eval() << std::endl;
	
	
	
	return 0;
}
