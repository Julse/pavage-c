#include <iostream>
#include "meta.hpp"

//---------------------------------------
// DEFINITIONS DE BASES

class OO;

template<class OO>
class PatronVisiteur
{
	void visitObjet(OO *o) {};
};

template<class TList, template<class> class PVisiteur>
class VisiteurMultiple;

template<typename TH, typename TL, template<class> class PVisiteur>
class VisiteurMultiple<TypeList<TH, TL>, PVisiteur> : 
	public PatronVisiteur<TH>,  
	public VisiteurMultiple<TL,PVisiteur>  {};

template <class T, template <class> class PVisiteur> 
class VisiteurMultiple<TypeList<T,NullType>, PVisiteur> :
	public PVisiteur<T> {};

//----------------------------------------
// DEFINITIONS DES SPECIALISATIONS

class O1;
class O2;

template<>
class PatronVisiteur<O1>
{
	public:
	void visitObjet1(O1 *o)
	{
		std::cout << "Visite de O1" << std::endl;
	}
};

template<>
class PatronVisiteur<O2>
{
	public:
	void visitObjet2(O2 *o)
	{
		std::cout << "Visite de O2" << std::endl;
	}
};

//----------------------------------------
// APPLICATION D'UN CRTP

typedef VisiteurMultiple<TypeList<O1, TypeList<O2, NullType> >, PatronVisiteur> Visiteur;

//----------------------------------------
// DEFINITIONS DE LA HIERACHIE DE CLASSE

class OO
{
	public:
		virtual void accept(Visiteur& v) = 0;
};

class O1 : public OO
{
	public:
		virtual void accept(Visiteur& v) { v.visitObjet1(this); }
};

class O2 : public OO
{
	public:
		virtual void accept(Visiteur& v) { v.visitObjet2(this); }
};


