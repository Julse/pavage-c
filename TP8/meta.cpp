#include <iostream>
#include <typeinfo>
#include "meta.hpp"

int main() {
	std::cout << Length<TypeList<int, TypeList<double, TypeList<float, NullType>>>>::valeur << std::endl;
	std::cout << typeid(TypeAt<TypeList<int, TypeList<double, TypeList<float, NullType>>>, 1>::tp).name() << std::endl;
	
	TypeAt<TypeList<int,  TypeList<int, NullType>>, 1>::tp z = 3;
	
	std::cout << z*15 <<std::endl;
	
	std::cout <<  IndexOf<TypeList<int,  TypeList<double, NullType>>, float>::valeur << std::endl;
}
