#ifndef META
#define META

#include <iostream>

template<typename T1, typename TL>
struct TypeList {
	typedef T1 Head;
	typedef TL Tail;
};
struct NullType {};


template<typename L>
struct Length;

template<typename TH, typename TL>
struct Length< TypeList<TH, TL> > {
	static const int valeur = 1 + Length<TL>::valeur;
};

template<>
struct Length< NullType > {
	static const int valeur = 0;
};

/////////////////////////////////////////::::///:::::::

template<typename L, int N>
struct TypeAt;

template<typename TH, typename TL, int N>
struct TypeAt< TypeList<TH, TL>, N> {
	typedef typename TypeAt<TL, N-1>::tp tp;
};

template<typename TH, typename TL>
struct TypeAt< TypeList<TH, TL>, 0 > {
	typedef TH tp;
};

template<int N>
struct TypeAt< NullType, N > {
	typedef NullType tp;
};

/////////////////////////////////////////::::///:::::::

template<typename L, typename T>
struct IndexOf;

template<typename TH, typename TL, typename T>
struct IndexOf< TypeList<TH, TL>, T> {
	static const int temp = IndexOf<TL, T>::valeur;
	static const int valeur = temp == -1 ? -1 : 1 + temp;
};

template<typename TH, typename TL>
struct IndexOf< TypeList<TH, TL>, TH> {
	static const int valeur = 1;
};

template<typename T>
struct IndexOf< NullType, T > {
	static const int valeur = -1;
};


#endif
