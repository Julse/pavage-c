#include <cstddef>
#include <iostream>
#include <cstdlib>
#include <cassert>

template<typename T, std::size_t S>
class Tableau
{
	private:
		T* data;
		
		std::size_t nb;
		std::size_t capacite;
		static unsigned int nb_instances;
	
	public:
		Tableau() : nb(0), capacite(S) 
		{
			data = new T[S];
			++nb_instances;
		}
		
		Tableau(const T t[], std::size_t taille) : nb(taille), capacite(taille+S)
		{
			data = new T[capacite];
			for(int i=0; i<nb; i++){
				data[i] = t[i];
			}
			++nb_instances;
		}
		
		Tableau(const Tableau<T, S>& autre) : nb(autre.nb), capacite(autre.capacite)
		{
			data = new T[capacite];
			for(int i=0; i<nb; i++){
				data[i] = autre[i];
			}
			++nb_instances;
		}
		
		void ajoute(T element);
		
		void augmente_capacite();
		
		std::size_t get_nb() const {
			return nb;
		}
		
		std::size_t get_capacite() const {
			return capacite;
		}
		
		unsigned int get_instances() const {
			return nb_instances;
		}
		
		const T* get_data() const {
			return data;
		}

		T& operator[](unsigned int index)
		{
			if (index > nb)
			{
				std::cout << "Out of range !!" << std::endl;
				abort();
			}
			return data[index];
		}
		
		const T operator[](unsigned int index) const
		{
			if (index > nb)
			{
				std::cout << "Out of range !!" << std::endl;
				abort();
			}
			return data[index];
		}
		
		Tableau<T, S>& operator=(const Tableau<T, S>& autre) 
		{
			if(autre.capacite>capacite){
				delete[] data;
				data = new T[autre.capacite];
				capacite = autre.capacite;
				nb = autre.nb;
				for(int i=0; i<autre.nb; i++){
					data[i] = autre.data[i];
				}
			}  
			else {
				nb = autre.nb;
				for(int i=0; i<autre.nb; i++){
					data[i] = autre.data[i];
				}
			}
			
			return *this;
		}
		
		~Tableau(){
			std::cout << "destruction" << std::endl;
			delete[] data;
			--nb_instances;
		}
};

template<typename T, std::size_t S>
void Tableau<T, S>::ajoute(T element){
	if(nb>=capacite){
		augmente_capacite();
	}  
	//Juste pour essayer
	assert(nb<capacite);
	
	std::cout << "ajout de " << element << std::endl;
	data[nb]=element;
	nb++;
}

template<typename T, std::size_t S>
unsigned int Tableau<T, S>::nb_instances = 0;

template<typename T, std::size_t S>
void Tableau<T, S>::augmente_capacite(){
		std::cout << "reallocation" << std::endl;
		T* tmp_data = data;
		data = new T[nb+S];
		capacite = nb+S;
		for(int i=0; i<nb; i++){
			data[i] = tmp_data[i];
		}
		delete tmp_data;
}

template<typename T, std::size_t N>
std::ostream & operator << (std::ostream & os, const Tableau<T,N>& tab) {
	for (int i=0; i < tab.get_nb(); i++)
		os << tab.get_data()[i] << " ";
	os << std::endl;
	return os;
}


template<typename T, std::size_t N>
std::istream & operator >> (std::istream & is, Tableau<T,N>& tab) {
	for (int i=0; i < tab.get_nb(); i++)
		is >> tab.get_data()[i];

	return is;
}

int main() {
	Tableau<int, 5> tab;
	tab.ajoute(5);
	tab.ajoute(2);
	tab.ajoute(5);
	tab.ajoute(2);
	tab.ajoute(5);
	tab.ajoute(2);
	
	std::cout << tab;
	
	Tableau<int, 5> t2(tab.get_data(), tab.get_nb());
	std::cout << t2 << std::endl;

	std::cout << "NOMBRE D'INSTANCES : " << t2.get_instances() << std::endl;
	
	std::cout << tab[4] << std::endl;
	tab[4] = 8;
	std::cout << tab[4] << std::endl;
	//std::cout << t2[8] << std::endl;
	
	Tableau<int, 5> tabcopie(tab);
	std::cout << tabcopie << std::endl << std::endl;

	std::cout << "NOMBRE D'INSTANCES : " << tab.get_instances() << std::endl;
	
	tab.ajoute(3);
	t2 = tab;

	std::cout << tab << std::endl;
	std::cout << t2 << std::endl;

}
