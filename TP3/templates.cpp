#include <iostream>
#include <cstring>


template<typename T>
void swap(T& a, T& b)
{
	T tmp = a;
	a = b;
	b = tmp;
}

template<typename T, unsigned int N>
void swap(T(&a)[N], T(&b)[N])
{
	for(int i=0; i < N; i++) {
		swap(a[i], b[i]);
	}
} // Utile pour swapper le contenu de deux tableaux statiques




template<typename T>
const T& min(const T& a, const T& b)
{
	std::cout << "<<1>>";
	if ( a <= b ) return a;
	else return b;
} // Gere le cas general

const char* const& min(const char* const& a, const char * const& b)
{
	std::cout << "<<2>>";
	if (strcmp(a,b)<=0) return a;
	else return b;
} // String literals a la c++

char* const& min(char* const& a, char * const& b)
{
	std::cout << "<<3>>";
	if (strcmp(a,b)<=0) return a;
	else return b;
} // String literals a la c ou en tableau

int main() {
	int p = 3, m = 7;
	swap(p, m);
	
	
	char a[] = "aaa";  const char * ts="trytytr"; const char * ta="527278"; 
	char b[] = "bbb";
	char cc[5]="tutu";
	
	std::cout << a << b << std::endl;
	//swap(a, b);
	std::cout << a << b << std::endl;
	
	
	std::cout  << "min(a,b) : " << min(a,b) << std::endl;
	int c=1;
	int d=2;
	
	std::cout  << "min(c,d) : " << min(c,d) << std::endl;
	
	std::cout << 	min("toto", "tata")  << std::endl;
	
	std::cout << 	min(ts, ta)  << std::endl;


}
