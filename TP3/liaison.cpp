#include <iostream>
#include <typeinfo>

class Employe
{ 
	public :
		virtual void affiche(){};
		virtual int age_retraite(){return 66;};
		virtual void augmentation(int){};
		virtual void salaire() {};
};

class Cadre : public Employe
{ 
	public :
		void affiche(){
			std::cout << "??" << std::endl;
		}
		int age_retraite(int age) {
			return age+42;
		}
		void augmentation(double aug){
		}
};

int main()
{
	Employe * pe = new Cadre;
	pe->affiche();
	pe->age_retraite();
	//pe->age_retraite(2);
	pe->augmentation(5.5);
	pe->salaire();
	Cadre *pc = static_cast<Cadre *>(pe);
	pc->affiche();
	//pc->age_retraite();
	pc->age_retraite(2);
	pc->augmentation(5.5);
	pc-> salaire();

	
	std::cout << "pc :" << typeid(pc).name() << std::endl;
	std::cout << "pc* :" << typeid(*pc).name() << std::endl;
	std::cout << "pe :" << typeid(pe).name() << std::endl;
	std::cout << "pe* :" << typeid(*pe).name() << std::endl;

	delete pe;
	return 0;
}


