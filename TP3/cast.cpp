double d;
const double r;
const double pi = 3.1416;
double *ptr = const_cast<double*>(&pi);
double *const cpt;
double *const ptd = &d;
const double *ctd = &d;
const double *ptc = &pi;
double *const ptp = &pi;
const double *const ppi = &pi;
double *const *pptr1 = const_cast<double*>(&ptc);
double *const *pptr2 = &ptd;

void F()
{
	ptr = new double;
	*(const_cast<double*>(&r)) = 1.0;
	*ptr = 2.0;
	//const_cast<double*>(cpt) = new double;
	//*cpt = 3.0;
	double* t = const_cast<double*>(ptc);
	t = new double;
	*t = 4.0;
	// Pareil pour le reste....
	ptd = new double;
	*ptd = 5.0;
	ctd = new double;
	*ctd = 6.0;
	ptp = new double;
	*ptp = 7.0;
	ppi = new double;
	*ppi = 8.0;
}
