#include <iostream>

class ProduitFrais;
class ProduitLuxe;
class ProduitLuxeHorsTaxe;

class Produit 
{ 
	private:
	int prix;
	
	public:
	virtual Produit & operator=(const Produit &)=0; 
	virtual void estAffecteA(ProduitFrais &) const =0;
	virtual void estAffecteA(ProduitLuxe &) const = 0;
	virtual void estAffecteA(ProduitLuxeHorsTaxe &) const =0;
	  
}; 

class ProduitFrais : public Produit 
{ 
	private:
	int peremption; 
	
	public:
	ProduitFrais & operator=(const Produit & opdroit);
	void estAffecteA(ProduitFrais &) const;
	void estAffecteA(ProduitLuxe &) const;
	void estAffecteA(ProduitLuxeHorsTaxe &) const;

};

class ProduitLuxe : public Produit 
{ 
	private:
	int swagpoint;

	public:
	ProduitLuxe& operator = (const Produit &); 	
	void estAffecteA(ProduitFrais &) const;
	void estAffecteA(ProduitLuxe &) const;
	void estAffecteA(ProduitLuxeHorsTaxe &) const;

	
};

class ProduitLuxeHorsTaxe : public ProduitLuxe
{ 
	public:
	ProduitLuxeHorsTaxe & operator = (const Produit &); 	
	void estAffecteA(ProduitFrais &) const;
	void estAffecteA(ProduitLuxe &) const;
	void estAffecteA(ProduitLuxeHorsTaxe &) const;


};

ProduitFrais & ProduitFrais::operator = (const Produit & opdroit)
{
	opdroit.estAffecteA(*this); return *this;
} 	

void ProduitFrais::estAffecteA(ProduitFrais &) const
{
	std::cout << "Frais => Frais" << std::endl;
}

void ProduitFrais::estAffecteA(ProduitLuxe &) const
{
	std::cout << "Frais => Luxe" << std::endl;
}

void ProduitFrais::estAffecteA(ProduitLuxeHorsTaxe &) const
{
	std::cout << "Frais => LuxeHT" << std::endl;
}

ProduitLuxe & ProduitLuxe::operator = (const Produit & opdroit)
{
	opdroit.estAffecteA(*this); return *this;
} 	

void ProduitLuxe::estAffecteA(ProduitFrais &) const
{
	std::cout << "Luxe => Frais" << std::endl;
}

void ProduitLuxe::estAffecteA(ProduitLuxe &) const
{
	std::cout << "Luxe => Luxe" << std::endl;
}

void ProduitLuxe::estAffecteA(ProduitLuxeHorsTaxe &) const
{
	std::cout << "Luxe => LuxeHT" << std::endl;
}

ProduitLuxeHorsTaxe & ProduitLuxeHorsTaxe::operator = (const Produit & opdroit)
{
	opdroit.estAffecteA(*this); return *this;
} 	

void ProduitLuxeHorsTaxe::estAffecteA(ProduitFrais &) const
{
	std::cout << "LuxeHT => Frais" << std::endl;
}

void ProduitLuxeHorsTaxe::estAffecteA(ProduitLuxe &) const
{
	std::cout << "LuxeHT => Luxe" << std::endl;
}

void ProduitLuxeHorsTaxe::estAffecteA(ProduitLuxeHorsTaxe &) const
{
	std::cout << "LuxeHT => LuxeHT" << std::endl;
}


int main(){
Produit *frais= new ProduitFrais() ; 
Produit *luxe = new ProduitLuxe() ; 
Produit *luxeHT = new ProduitLuxeHorsTaxe() ; 

*frais = *luxe;
*luxe = *luxeHT;
*luxeHT = *frais;
}
